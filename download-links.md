<img src="https://atomjump.com/images/logo80.png">



# medimage-co-nz-content-only
This repository is a public copy of the important content on http://medimage.co.nz,  in case the servers should be down.

## Apps

__Standard (messaging-based)__

iOS / Android / Desktop :   On your device, tap the button below to start the app, and then tap ‘Install’ to add it to your homepage. See these iPhone or Android videos if you are stuck. Phone Requirements (phone-requirements.md)

* http://medimage.co.nz/installation-videos/#iphone-standard-app
* http://medimage.co.nz/installation-videos/#android-standard-app

Start app from:
http://ajmp.co/

Note for returning users: the app-store apps have been discontinued. The Professional app, below, is it’s replacement.

__Professional (photo-only)__

iOS / Android / Desktop :  On your device, tap the button below to start the app, and then tap the ‘Bookmark’ button in the bottom left-hand corner to add it to your homepage. See these iPhone or Android videos if you are stuck. Phone Requirements

* http://medimage.co.nz/installation-videos/#iphone-prof-app
* http://medimage.co.nz/installation-videos/#android-prof-app

Start app from:
https://medimage-app.atomjump.com/

Note for returning users: the app-store apps have been discontinued.


## MedImage Server

* Windows 64-bit
https://medimage-download.atomjump.com/MedImageInstaller.exe

* Windows 32-bit 
https://medimage-download.atomjump.com/MedImageInstaller32.exe

* For Mac
http://medimage.co.nz/medimage-server-mac-installation/
Or https://src.atomjump.com/atomjump/medimage-co-nz-content-only/blob/master/mac-installation.md

* For linux
https://www.npmjs.com/package/medimage


## Addons

* EHR Connector

https://medimage-download.atomjump.com/MedImageEHR64Installer.exe
https://medimage-download.atomjump.com/MedImageEHR32Installer.exe

https://medimage-download.atomjump.com/medimage-addons/medimage-addon-ehr-medtech32-0.5.2.zip

MedTech Evolution Versions < 6.3
https://medimage-download.atomjump.com/medimage-addons/medimage-addon-ehr-medtech-evolution-0.1.1.zip

MedTech Evolution Versions >= 6.3
https://medimage-download.atomjump.com/medimage-addons/medimage-addon-ehr-medtech-evolution-0.2.0.zip

https://medimage-download.atomjump.com/medimage-addons/medimage-addon-ehr-atomjump-messaging-0.1.7.zip



* Wound Mapp 

https://medimage-download.atomjump.com/WoundInstaller.exe
https://medimage-download.atomjump.com/Wound32Installer.exe
https://medimage-download.atomjump.com/wound-1.7.8.zip

* MedImage Export

https://frontcdn.atomjump.com/atomjump-messaging/medimage_export-0.3.3.zip

* MedImage App Server

https://frontcdn.atomjump.com/medimage-app-server-2.4.8.zip


## Cloud Server Options

This product is also known as a ‘MedImage Proxy Server’.

* For linux
https://www.npmjs.com/package/medimage

* Windows 64-bit
https://medimage-download.atomjump.com/MedImageInstaller-cloud.exe

* Windows 32-bit 
https://medimage-download.atomjump.com/MedImageInstaller32-cloud.exe

After installation of the Windows binaries, you should make changes to the configuration file. Please see the technical-guide.md file, section "Windows Binary Cloud Setup".


## Latest Beta

* Windows 64-bit:
https://medimage-download.atomjump.com/MedImageInstaller-beta.exe

* Windows 32-bit:
https://medimage-download.atomjump.com/MedImageInstaller-beta32.exe

* Cloud (Developer 2.0 preview)

https://medimage-download.atomjump.com/MedImageInstaller-beta-cloud.exe
https://medimage-download.atomjump.com/MedImageInstaller-beta32-cloud.exe



## Upgrading Notes

Specific Version notes

If you are using a version of the MedImage Server < 1.9.0 and you are upgrading to Ver >= 2.0.0, you will also need to upgrade your add-ons, e.g. the EHR Connector, to their matching 2.0 versions.
