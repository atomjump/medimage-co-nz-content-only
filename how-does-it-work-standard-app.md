
# medimage-co-nz-content-only
This repository is a public copy of the important content on http://medimage.co.nz,  in case the servers should be down.

# How does it work? Standard App


## How does the image/PDF get sent?

The MedImage standard app:

* The image is shrunk to 1200 pixels wide (about 400Kb), and uploaded over an encrypted internet connection, via Wifi, or a mobile connection to the private messaging server hosted at ajmp.co.  It becomes immediately visible to those users who have been invited on that forum, only. This takes about 1 second.
* A doctor may have already established a pairing between their instance of ajmp.co messaging, and their PC.  If so, the photos will automatically be registered to download within 10 seconds. If not, either party can select the message manually for download to their local MedImage server. They can also choose to download all photos and messages in one combined PDF.
* It will transfer down to the local PC via the PC’s own internet connection. Depending on your internet speed, this typically takes around 10-20 seconds total.



## How does the pairing work?

MedImage uses a unique code which is only valid for one PC server. This unique code is unguessable and only available for 1 hour. Once it is generated, any number of your phones can be used to attach to that local PC.

Once the pairing is complete for a phone, you do not have to do any pairing in future. You can still choose to pair once more if there is a configuration change, however.
