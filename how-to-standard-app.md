<img src="https://atomjump.com/images/logo80.png">



# medimage-co-nz-content-only
This repository is a public copy of the important content on http://medimage.co.nz,  in case the servers should be down.

# How To (Standard App)


## Pair a phone with your PC

Assuming you have installed both your MedImage server on your PC and have installed the standard MedImage app on your phone.

1. Open your MedImage server on your PC
2. Click on the large ‘pairing buttons’ (you will need to use either AJ Server pairing, or Your Server Pairing for the standard app):
3. With the generated code, enter this into your phone app. If this is the first time you have used the app, tap ‘New session‘, then ‘Open forum’, and enter a comment “start medimage” in the comment box at the top and tap ‘Send’. Then enter a comment “pair [your 4 digit code]”, in this case “pair 3bwC”. Note: the commands entered are not case sensitive.


 

## Take a photo with your app

In your app:

1. In your app, enter an identifier for your patient with a comment “id [your unique patient ID, and optional description]”, e.g. “id NGF4367 sting”
2. Click the upload button at the bottom of the window. Where it says “Upload > Choose Files”, or similar, push the ‘Choose Files‘ button. Depending on your phone type, you will likely be given a selection of your gallery photos or your ‘Camera’. For secure photos which do not get left on the phone, select ‘Camera’, but you can also use older gallery photos.
3. Snap a photo of the wound, and confirm.
4. You will then get a thumbnail preview and a larger preview, below it. Tap ‘Upload‘ if it is suitable to be entered onto the forum.
5. You will see the photo on the forum. Wait for the photo to auto-transfer to your PC, if you are paired.


On your PC:

1. Open up your MedImage folder, which is where you set it during installation (it defaults to your desktop, and will be created in a few seconds, if it isn’t there already. If this doesn’t appear, look in C:\MedImage\Photos)
2. Go to the subfolder of your patient ID
3. You will see the patient’s photo, already named



## Create a folder, or create more advanced directories/filenames

You can prepend a ‘#’ to any word in the ID field of your app, and it will create a folder. The folder will be created if it doesn’t exist, or add to it, if it already exists, so nothing is ever removed.

Here are some examples of what to enter in the ID field:

```
Enter                        Created on your PC
file                         \file-datetime.jpg
#folder                      \folder\datetime.jpg
#folder file                 \folder\file-datetime.jpg
#folder #subfolder file      \folder\subfolder\file-datetime.jpg
file fileagain               \file-fileagain-datetime.jpg
[blank]                      \image-datetime.jpg
```


## Export a single image

You will likely need to do this manually if your patient has added the photo, rather than yourself.

* Within the forum, tap on a message that includes an image (in the blank space to the right of the image). Note: tapping the image itself will only show you a zoomable version of that image.
* Tap the ‘MedImage’ icon underneath the image (next to the rubbish bin). You may need to scroll downwards.


## Export a PDF of a conversation

1. Tap the ‘Upload’ button from within the messaging window.
2. Tap the ‘MedImage’ icon to export a PDF file of the conversation to MedImage. Note: tapping the PDF icon itself will only show the PDF in your browser.

