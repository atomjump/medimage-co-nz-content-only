<img src="https://atomjump.com/images/logo80.png">



# medimage-co-nz-content-only
This repository is a public copy of the important content on http://medimage.co.nz,  in case the servers should be down.



## MedImage App Phone Requirements
As a general rule of thumb: any phone or device from around 2017, onwards, should be able to run the MedImage apps.

* The phone must be able to run a modern browser app, “Javascript” must be enabled, and (“1st party”) cookies must be enabled.
* There must be enough RAM on the phone to operate the browser, and some basic web functionality on top. Usually this minimum requirement would be > 1GB of RAM, and > 100MB free memory space on the phone.
* Any browser which supports modern Javascript should be usable. Options on iPhones/iPads include Safari, Chrome, Firefox. Options on Android phones/devices include Chrome, Samsung Internet, UC, Brave, Firefox (the Fennec build installed via F-Droid, rather than Google Play), FOSS, Opera.

### Troubleshooting

* If you are receiving out-of-memory errors when trying to upload a photo, or tapping the large MedImage camera button does nothing, it may be caused by the Android phone manufacturer disabling file uploads in browsers. See the solution a), below.
* In some browsers, notably Firefox installed via Google Play, the main button does not bring up the camera view, but rather a gallery/file selection. You can still take the photo using your ordinary camera app, and select this photo from here, although this is clearly not as convenient. You can also install Firefox Fennec via F-Droid, which does include a working camera (we have had some issues with the shortcut opening the browser correctly, however – you may want to set the page as your browser homepage for consistent opening).
* One tip to minimize your memory usage: when asked “to permanently store data on my device” when you first run the web page, you can answer ‘no’. This would likely streamline the RAM requirements, although there are some potential, rare, moments where the app may forget photos taken, so please use this with caution. If you’ve already answered yes, you can clear this setting off by clearing the browser cache/history for that page. See solution b), below.

### Solutions

a) To solve this issue, you need to enable “Developer mode” for Android, and then, in Settings > Developers Options tick off “Do not keep activities”. A video and discussion of this is available here.

b) More information on the “Permanent Storage of data” option on Android phones is available here.

